q# Quiz (quiz)

A Quiz web application is general purpose quiz
[Free software](https://www.gnu.org/philosophy/free-sw.html) application
licenced under [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)
or later and based on the [Quasar framework](https://quasar.dev/),
([Vue.js version 3](https://vuejs.org/),
[TypeScript](https://www.typescriptlang.org/),
[TOML](https://toml.io)).

Tested with Node version

```bash
root@63a1a367d282:/quiz-app$ node --version
v18.16.0
```

## Install the dependencies

Quasar framework CLI

```bash
yarn global add @quasar/cli npx
```

Application dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

Adjust Quiz application config via [TOML](https://toml.io/en/) config file
`./quiz_config.toml` or via SHELL/ENV variables.

Application global vars defined inside config file `./quiz_config.toml`
are overrided by SHELL/ENV vars (defined here
`$EDITOR ./src/utils/get_app_conf.js`).

When you add some app new global var inside config file
`./quiz_config.toml` you must add correspoding SHELL/ENV var here
`./src/utils/get_app_conf.js`.

```bash
PRIMARY_COLOR=
SECONDARY_COLOR=
TITLE_IMAGE=
TITLE_IMAGE_WIDTH=
TITLE_IMAGE_HEIGTH=
QUIZ_IMAGE=
QUIZ_CARD_WIDTH=
QUIZ_TITLE=
QUIZ_SUBTITLE=
ANSWER_PER_QUESTION=
CORRECT_ANSWER=
SHOW_QUIT_APP_BTN=
```

![Quiz app](./doc/quiz.png)

Launch development server with

```bash
npx qusar dev
```

### Lint the files

```bash
yarn lint
```

### Format the files

```bash
yarn format
```

### Run app tests

```bash
# Component tests with default Electron web browser
npx cypress run --component
# or interactively
npx cypress open --component

# Component tests with Mozilla Firefox web browser
npx cypress run --component --browser $(which firefox-bin)
# or interactively
npx cypress open --browser $(which firefox-bin)

# e2e and unit tests with default Electron web browser
npx cypress run --e2e
# or interactively
npx cypress open --e2e

# e2e and unit tests with Mozilla Firefox web browser
npx cypress run --e2e --browser $(which firefox-bin)
# or interactively
npx cypress open --browser $(which firefox-bin)

```

[Use another web browsers.](https://docs.cypress.io/guides/guides/launching-browsers)

### Develoment app with build Docker image/container

```bash
APP_NAME=quiz-app
APP_DIR=/home/dev/$APP_NAME

# Build Docker image
docker buildx build --build-arg="UID=$(id -u)" -f ./docker/dev/Dockerfile .

# Run Docker app container
docker run -it --rm -e "PS1=\u@\h:\w$ " -p 9000:9000 -v $(pwd):$APP_DIR --name $APP_NAME <YOUR_BUILDED_DOCKER_IMAGE_ID>

# Or if you want override some Docker app container ENV variables (-e flag)
docker run -it --rm -e "PS1=\u@\h:\w$ " -e "PRIMARY_COLOR=red" -p 9000:9000 -v $(pwd):$APP_DIR --name $APP_NAME <YOUR_BUILDED_DOCKER_IMAGE_ID>

# Run quasar app dev server from emulator terminal
dev@908b331893bf:~/quiz-app$ npx quasar dev

# Check web app from your host OS via web browser
# Open default web browser with URL http://localhost:9000 from host OS emulator terminal
test@test:~$ xdg-open http://localhost:9000
```

### Build the app for production

[Check application config via config file or SHELL/ENV var.](<#start-the-app-in-development-mode-(hot-code-reloading,-error-reporting,-etc.)>)

Build application with

```bash
npx quasar build
```

Path of builded application is `./dist/spa/`.

### Incorporate Quiz web application into another web page

Add this link into your web HTML page source code

```html
<a href="https://tmszi.codeberg.page/quiz/#/" target="_blank" rel="noopener noreferrer">Quiz application</a>
```

With incorporate this link into another web page it is possible quit
Quiz application web browser tab with quit button widget (show quit
button via global settings `showQuitAppBtn` var).


### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

### Licence

[GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) or later.
