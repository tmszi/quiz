import {
  hexToRgb,
  transparentColor,
  whiteColor,
} from '../../../test/cypress/utils/';

beforeEach(() => {
  cy.visit(Cypress.config('baseUrl'));
  cy.window().should('have.property', 'i18n');
});

describe('Home page', () => {
  it('render <q-card>', () => {
    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('q-card')
        .should('be.visible')
        .and('have.css', 'width', config.quizCardWidth);
      //.matchImageSnapshot();
    });
  });

  it('render <q-card-section-title>', () => {
    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('q-card-section-title')
        .should('be.visible')
        .and('have.class', 'bg-primary', 'text-white')
        .and('have.css', 'background-color', hexToRgb(config.primaryColor));
    });
  });

  it('render <quiz-title>', () => {
    cy.task('getAppConfig', process).then((config) => {
      let i18n;
      cy.window()
        .then((win) => {
          i18n = win.i18n;
        })
        .then(() => {
          cy.dataCy('quiz-title')
            .should('be.visible')
            .and('have.class', 'text-h6')
            .and('have.text', i18n.global.t(config.quizTitle))
            .and('have.css', 'color', whiteColor);
        });
    });
  });

  it('render <quiz-switch-lang>', () => {
    let i18n;
    cy.window()
      .then((win) => {
        i18n = win.i18n;
      })
      .then(() => {
        cy.dataCy('quiz-switch-lang')
          .should('be.visible')
          .children()
          .should('have.length', Object.keys(i18n.global.messages).length);

        cy.task('getAppConfig', process).then((config) => {
          cy.dataCy('quiz-switch-lang')
            .children(`#${i18n.global.locale}-lang`)
            .and('have.text', i18n.global.locale)
            .and(
              'have.css',
              'background-color',
              hexToRgb(config.secondaryColor)
            );
        });
      });
  });

  it('render <quiz-title-circle-div>', () => {
    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('quiz-title-circle-div')
        .should('be.visible')
        .and('have.class', 'circle', 'float.right')
        .and('have.css', 'width', config.titleImageWidth)
        .and('have.css', 'height', config.titleImageHeight)
        .and('have.css', 'background-color', whiteColor);
    });
  });

  it('render <quiz-title-img>', () => {
    cy.visit(Cypress.config('baseUrl'));
    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('quiz-title-img')
        .should('be.visible')
        .get('img')
        .should(($img) => {
          // "naturalWidth" and "naturalHeight" are set when the image loads
          expect($img[0].naturalWidth).to.be.greaterThan(0);
        })
        .invoke('attr', 'src')
        .should('contain', config.titleImage);

      cy.dataCy('quiz-title-img').matchImageSnapshot({
        failureThreshold: 0.1, // threshold for entire image
        failureThresholdType: 'percent', // percent of image or number of pixels)
      });
    });
  });

  it('render <quiz-subtitle>', () => {
    let i18n;
    cy.window()
      .then((win) => {
        i18n = win.i18n;
      })
      .then(() => {
        cy.task('getAppConfig', process).then((config) => {
          cy.dataCy('quiz-subtitle')
            .should('be.visible')
            .and('have.class', 'text-subtitle2')
            .and('have.text', i18n.global.t(config.quizSubtitle))
            .and('have.css', 'color', whiteColor);
        });
      });
  });

  it('render <q-linear-progress>', () => {
    cy.dataCy('q-linear-progress')
      .should('be.visible')
      .should('have.css', 'height', '15px')
      .get('div.q-badge')
      .should('have.text', '0%');
  });

  it('render <q-btn-quit-app>', () => {
    let i18n;
    cy.window()
      .then((win) => {
        i18n = win.i18n;
      })
      .then(() => {
        cy.task('getAppConfig', process).then((config) => {
          cy.dataCy('q-btn-quit-app')
            .should('be.visible')
            .should(
              'have.class',
              'absolute-bottom-right',
              'q-mr-md',
              'q-mb-md',
              'bg-primary'
            )
            .and('not.be.disabled')
            .and('have.css', 'background-color', hexToRgb(config.primaryColor))
            .get('i')
            .should('have.text', 'logout');
        });
        // Btn tooltip
        cy.dataCy('q-btn-quit-app').eq(0).invoke('show').trigger('mouseenter');
        cy.dataCy('q-btn-quit-app-tooltip').should(
          'have.text',
          i18n.global.t('quitAppBtnTooltip')
        );
      });
  });

  it('render <q-card-section-question> next btn', () => {
    let i18n;
    cy.window()
      .then((win) => {
        i18n = win.i18n;
      })
      .then(() => {
        cy.task('getAppConfig', process).then((config) => {
          let questionIndex = 0;
          // Question
          cy.dataCy('q-card-section-question')
            .should('be.visible')
            .get('div.text-h5')
            .should(
              'have.text',
              i18n.global.t(
                config.quiz[questionIndex * config.answersPerQuestion].question
              )
            );
          // Question answers radio btn
          cy.get('[type="radio"]')
            .should(
              'have.length',
              config.quiz.length / config.answersPerQuestion
            )
            .each(($el, answerIndex) => {
              if (
                config.quiz[
                  questionIndex * config.answersPerQuestion + answerIndex
                ].value !== config.correctAnswer
              ) {
                cy.wrap($el)
                  .click({ force: true })
                  .parent()
                  .parent()
                  .parent()
                  .should('have.css', 'background-color', transparentColor)
                  .contains(
                    i18n.global.t(
                      config.quiz[
                        questionIndex * config.answersPerQuestion + answerIndex
                      ].label
                    )
                  );
              } else {
                // Correct answer
                cy.wrap($el)
                  .click({ force: true })
                  .parent()
                  .parent()
                  .parent()
                  .should(
                    'have.css',
                    'background-color',
                    hexToRgb(config.secondaryColor)
                  )
                  .children()
                  .contains(
                    i18n.global.t(
                      config.quiz[
                        questionIndex * config.answersPerQuestion + answerIndex
                      ].label
                    )
                  );
              }
            });
          // Probressbar
          cy.dataCy('q-linear-progress')
            .should('be.visible')
            .get('div.q-badge')
            .should(
              'have.text',
              `${
                (
                  (questionIndex * config.answersPerQuestion) /
                  config.quiz.length
                ).toFixed(2) * 100
              }%`
            );

          // Back btn
          if (questionIndex === 0) {
            cy.dataCy('q-btn-back')
              .should('be.visible')
              .and('be.disabled')
              .and('have.text', i18n.global.t('backBtn'));
          } else {
            cy.dataCy('q-btn-back')
              .should('be.visible')
              .and('not.be.disabled')
              .and('have.text', i18n.global.t('backBtn'));
          }
          // Next btn
          cy.dataCy('q-btn-next')
            .should('be.visible')
            .and('not.be.disabled')
            .and('have.text', i18n.global.t('nextBtn'))
            .click({ force: true });

          cy.dataCy('q-linear-progress')
            .should('be.visible')
            .get('div.q-badge')
            .should('have.text', '33%');
        });
      });
  });

  it('render <q-img-quiz-img>', () => {
    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('q-img-quiz-img')
        .should('be.visible')
        .find('img')
        .should(($img) => {
          // "naturalWidth" and "naturalHeight" are set when the image loads
          expect($img[0].naturalWidth).to.be.greaterThan(0);
        })
        .invoke('attr', 'src')
        .should('contains', config.quizImage);
    });
  });

  it('switch language', () => {
    let i18n;
    cy.window()
      .then((win) => {
        i18n = win.i18n;
      })
      .then(() => {
        const otherLocales = Object.keys(i18n.global.messages);
        otherLocales.splice(otherLocales.indexOf(i18n.global.locale), 1);

        otherLocales.forEach((locale) => {
          cy.dataCy('quiz-switch-lang').children(`#${locale}-lang`).click();

          cy.task('getAppConfig', process).then((config) => {
            let questionIndex = 0;
            // Question
            cy.dataCy('q-card-section-question')
              .should('be.visible')
              .get('div.text-h5')
              .should(
                'have.text',
                i18n.global.t(
                  config.quiz[questionIndex * config.answersPerQuestion]
                    .question
                )
              );
            // Question answers radio btn
            cy.get('[type="radio"]')
              .should(
                'have.length',
                config.quiz.length / config.answersPerQuestion
              )
              .each(($el, answerIndex) => {
                if (
                  config.quiz[
                    questionIndex * config.answersPerQuestion + answerIndex
                  ].value !== config.correctAnswer
                ) {
                  cy.wrap($el)
                    .click({ force: true })
                    .parent()
                    .parent()
                    .parent()
                    .should('have.css', 'background-color', transparentColor)
                    .contains(
                      i18n.global.t(
                        config.quiz[
                          questionIndex * config.answersPerQuestion +
                            answerIndex
                        ].label
                      )
                    );
                } else {
                  // Correct answer
                  cy.wrap($el)
                    .click({ force: true })
                    .parent()
                    .parent()
                    .parent()
                    .should(
                      'have.css',
                      'background-color',
                      hexToRgb(config.secondaryColor)
                    )
                    .children()
                    .contains(
                      i18n.global.t(
                        config.quiz[
                          questionIndex * config.answersPerQuestion +
                            answerIndex
                        ].label
                      )
                    );
                }
              });
            // Probressbar
            cy.dataCy('q-linear-progress')
              .should('be.visible')
              .get('div.q-badge')
              .should(
                'have.text',
                `${
                  (
                    (questionIndex * config.answersPerQuestion) /
                    config.quiz.length
                  ).toFixed(2) * 100
                }%`
              );

            // Back btn
            if (questionIndex === 0) {
              cy.dataCy('q-btn-back')
                .should('be.visible')
                .and('be.disabled')
                .and('have.text', i18n.global.t('backBtn'));
            } else {
              cy.dataCy('q-btn-back')
                .should('be.visible')
                .and('not.be.disabled')
                .and('have.text', i18n.global.t('backBtn'));
            }
            // Next btn
            cy.dataCy('q-btn-next')
              .should('be.visible')
              .and('not.be.disabled')
              .and('have.text', i18n.global.t('nextBtn'))
              .click({ force: true });

            cy.dataCy('q-linear-progress')
              .should('be.visible')
              .get('div.q-badge')
              .should('have.text', '33%');
          });
        });
      });
  });
});
