### Add new locale

When you want add some new locale you must create new TOML file inside
`./src/i18n/` directory, with new locale/lang name e.g. sk.toml (Slovak locale).

If you change some question text or question answer label text inside global
config file `./quiz_config.toml` you must change corresponding
translattion key inside all locale config files e.g. en.toml, sk.toml.

Change some question text or question answer label text inside `./quiz_config.toml`

```toml
# First question

[[quiz]]
question = "What is the actual GRASS GIS stable release?"
label = "7.8.7" # first answer label
value = "7.8.7" # first answer value
transKey = "7.8.7"
```

change to

```toml
# First question

[[quiz]]
question = "Changed question"
label = "changed question label" # first answer label
value = "7.8.7" # first answer value
transKey = "7.8.7"
```

Change corresponding translation key inside all locale config files

`./src/i18n/en.toml`

```toml
# First question
"What is the actual GRASS GIS stable release?" = "What is the actual GRASS GIS stable release?"
# Answers
"7.8.7" = "7.8.7"
"8.0.0" = "8.0.0"
"8.2.0" = "8.2.0"
```

change to

```toml
# First question
"Changed question" = "Changed question"
# Answers
"changed question label" = "changed question label"
"8.0.0" = "8.0.0"
"8.2.0" = "8.2.0"
```
