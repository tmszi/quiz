import { i18n } from '../../boot/i18n';
import QuestionsQuiz from '../QuestionsQuiz.vue';
import {
  hexToRgb,
  transparentColor,
  whiteColor,
} from '../../../test/cypress/utils/';

describe('<QuestionsQuiz>', () => {
  it('render <q-card>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('q-card')
        .should('be.visible')
        .and('have.css', 'width', config.quizCardWidth);
      //.matchImageSnapshot();
    });
  });

  it('render <q-card-section-title>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('q-card-section-title')
        .should('be.visible')
        .and('have.class', 'bg-primary', 'text-white')
        .and('have.css', 'background-color', hexToRgb(config.primaryColor));
    });
  });

  it('render <quiz-title>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('quiz-title')
        .should('be.visible')
        .and('have.class', 'text-h6')
        .and('have.text', i18n.global.t(config.quizTitle))
        .and('have.css', 'color', whiteColor);
    });
  });

  it('render <quiz-switch-lang>', () => {
    cy.mount(QuestionsQuiz, {});

    const num_of_locales = Object.keys(i18n.global.messages).length;
    cy.dataCy('quiz-switch-lang')
      .should('be.visible')
      .children()
      .should('have.length', num_of_locales);

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('quiz-switch-lang')
        .children(`#${i18n.global.locale}-lang`)
        .and('have.text', i18n.global.locale)
        .and('have.css', 'background-color', hexToRgb(config.secondaryColor));
    });
  });

  it('render <quiz-title-circle-div>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('quiz-title-circle-div')
        .should('be.visible')
        .and('have.class', 'circle', 'float.right')
        .and('have.css', 'width', config.titleImageWidth)
        .and('have.css', 'height', config.titleImageHeight)
        .and('have.css', 'background-color', whiteColor);
    });
  });

  it('render <quiz-title-img>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('quiz-title-img')
        .should('be.visible')
        .get('img')
        .should(($img) => {
          // "naturalWidth" and "naturalHeight" are set when the image loads
          expect($img[0].naturalWidth).to.be.greaterThan(0);
        })
        .invoke('attr', 'src')
        .should('contain', config.titleImage);

      cy.dataCy('quiz-title-img').matchImageSnapshot({
        failureThreshold: 0.1, // threshold for entire image
        failureThresholdType: 'percent', // percent of image or number of pixels)
      });
    });
  });

  it('render <quiz-subtitle>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('quiz-subtitle')
        .should('be.visible')
        .and('have.class', 'text-subtitle2')
        .and('have.text', i18n.global.t(config.quizSubtitle))
        .and('have.css', 'color', whiteColor);
    });
  });

  it('render <q-linear-progress>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.dataCy('q-linear-progress')
      .should('be.visible')
      .should('have.css', 'height', '15px')
      .get('div.q-badge')
      .should('have.text', '0%');
  });

  it('render <q-btn-quit-app>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('q-btn-quit-app')
        .should('be.visible')
        .should(
          'have.class',
          'absolute-bottom-right',
          'q-mr-md',
          'q-mb-md',
          'bg-primary'
        )
        .and('not.be.disabled')
        .and('have.css', 'background-color', hexToRgb(config.primaryColor))
        .get('i')
        .should('have.text', 'logout');
    });
    // Btn tooltip
    cy.dataCy('q-btn-quit-app').eq(0).invoke('show').trigger('mouseenter');
    cy.dataCy('q-btn-quit-app-tooltip').should(
      'have.text',
      i18n.global.t('quitAppBtnTooltip')
    );
  });

  it('render <q-card-section-question> next btn', () => {
    cy.mount(QuestionsQuiz, {});
    cy.task('getAppConfig', process).then((config) => {
      cy.wrap([
        ...Array(config.quiz.length / config.answersPerQuestion).keys(),
      ]).each((questionIndex) => {
        /*
        Example:

        Questions:

        3 questions and every question has 3 answers -> 3 * 3 = 9 (config.quiz.length)

        [...Array(config.quiz.length / config.answersPerQuestion] 
        -> 9 / 3 = 3 
        -> questionIndex = [0, 1, 2] iterate over every questions config.quiz

        config.quiz[i * config.answersPerQuestion].question
        -> config.quiz[0 * 3].question, first question 
        -> config.quiz[1 * 3].question, second question 
        -> config.quiz[2 * 3].question, third question 

        Questions answers:

        3 answers per question (according config.answersPerQuestion)
        -> answerIndex = [0, 1, 2]

        config.quiz[i * config.answersPerQuestion + index].value
        -> config.quiz[0 * 3 + 0].value, first question with first answer
        -> config.quiz[0 * 3 + 1].value, first question with second answer
        -> config.quiz[0 * 3 + 2].value, first question with third answer
     
        -> config.quiz[1 * 3 + 0].value, second question with first answer
        -> config.quiz[1 * 3 + 1].value, second question with second answer
        -> config.quiz[1 * 3 + 2].value, second question with third answer
     
        -> config.quiz[2 * 3 + 0].value, third question with first answer
        -> config.quiz[2 * 3 + 1].value, third question with second answer
        -> config.quiz[2 * 3 + 2].value, third question with third answer 
        */

        // Image snapshot
        //cy.dataCy('q-card').matchImageSnapshot(`q-card-next-btn-question-${questionIndex}`);

        // Question
        cy.dataCy('q-card-section-question')
          .should('be.visible')
          .get('div.text-h5')
          .should(
            'have.text',
            i18n.global.t(
              config.quiz[questionIndex * config.answersPerQuestion].question
            )
          );

        // Question answers radio btn
        cy.get('[type="radio"]')
          .should('have.length', config.quiz.length / config.answersPerQuestion)
          .each(($el, answerIndex) => {
            if (
              config.quiz[
                questionIndex * config.answersPerQuestion + answerIndex
              ].value !== config.correctAnswer
            ) {
              cy.wrap($el)
                .click({ force: true })
                .parent()
                .parent()
                .parent()
                .should('have.css', 'background-color', transparentColor)
                .contains(
                  i18n.global.t(
                    config.quiz[
                      questionIndex * config.answersPerQuestion + answerIndex
                    ].label
                  )
                );
            } else {
              // Correct answer
              cy.wrap($el)
                .click({ force: true })
                .parent()
                .parent()
                .parent()
                .should(
                  'have.css',
                  'background-color',
                  hexToRgb(config.secondaryColor)
                )
                .children()
                .contains(
                  i18n.global.t(
                    config.quiz[
                      questionIndex * config.answersPerQuestion + answerIndex
                    ].label
                  )
                );
            }
          });
        // Probressbar
        cy.dataCy('q-linear-progress')
          .should('be.visible')
          .get('div.q-badge')
          .should(
            'have.text',
            `${
              (
                (questionIndex * config.answersPerQuestion) /
                config.quiz.length
              ).toFixed(2) * 100
            }%`
          );

        // Back btn
        if (questionIndex === 0) {
          cy.dataCy('q-btn-back')
            .should('be.visible')
            .and('be.disabled')
            .and('have.text', i18n.global.t('backBtn'));
        } else {
          cy.dataCy('q-btn-back')
            .should('be.visible')
            .and('not.be.disabled')
            .and('have.text', i18n.global.t('backBtn'));
        }
        // Next btn
        cy.dataCy('q-btn-next')
          .should('be.visible')
          .and('not.be.disabled')
          .and('have.text', i18n.global.t('nextBtn'))
          .click();
      });
    });
  });

  it('render <q-card-section-result>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      const correctAnsweredQuestions = 3;
      cy.dataCy('q-card-section-question-answers').should('not.be.visible');
      cy.dataCy('q-card-section-result').should('be.visible');
      cy.dataCy('q-card-section-result')
        .children()
        .children()
        .each(($el, index) => {
          if (index === 0) {
            cy.wrap($el).should(
              'have.text',
              `${correctAnsweredQuestions} ${i18n.global.t(
                'corectAnsweredQuestions',
                { count: correctAnsweredQuestions }
              )}`
            );
          } else if (index === 1) {
            cy.wrap($el).contains('/');
          } else if (index === 2) {
            cy.wrap($el).should(
              'have.text',
              `${config.answersPerQuestion} ${i18n.global.t('totalQuestions', {
                count: correctAnsweredQuestions,
              })}`
            );
          }
        });
    });
  });

  it('render <q-linear-progress>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.dataCy('q-linear-progress')
      .should('be.visible')
      .get('div.q-badge')
      .should('have.text', '100%');
  });

  it('render <q-img-quiz-img>', () => {
    cy.mount(QuestionsQuiz, {});

    cy.task('getAppConfig', process).then((config) => {
      cy.dataCy('q-img-quiz-img')
        .should('be.visible')
        .find('img')
        .should(($img) => {
          // "naturalWidth" and "naturalHeight" are set when the image loads
          expect($img[0].naturalWidth).to.be.greaterThan(0);
        })
        .invoke('attr', 'src')
        .should('contains', config.quizImage);
    });
  });

  it('render <q-card-section-question> back btn', () => {
    cy.mount(QuestionsQuiz, {});
    cy.task('getAppConfig', process).then((config) => {
      cy.wrap([
        ...Array(config.quiz.length / config.answersPerQuestion).keys(),
      ]).each((questionIndex) => {
        /*
        Example:

        Questions:

        3 questions and every question has 3 answers -> 3 * 3 = 9 (config.quiz.length)

        [...Array(config.quiz.length / config.answersPerQuestion] 
        -> 9 / 3 = 3 
        -> questionIndex = [0, 1, 2] iterate over every questions config.quiz

        calcQuestionIdx = Math.abs(questionIndex * config.answersPerQuestion 
          - quizConfig.quiz.length) - config.answersPerQuestion
        config.quiz[calcQuestionIdx].question
        -> config.quiz[Math.abs(0 * 3 - 9) - 3 = 6].question, third question 
        -> config.quiz[Math.abs(1 * 3 - 9) - 3 = 3].question, second question 
        -> config.quiz[Math.abs(2 * 3 - 9) - 3 = 0].question, first question 

        Questions answers:

        3 answers per question (according config.answersPerQuestion)
        -> answerIndex = [0, 1, 2]

        calcQuestionIdx = Math.abs((questionIndex * config.answersPerQuestion + answerIndex) 
        - (config.quiz.length - 1));
        config.quiz[calcQuestionIdx].value
        -> config.quiz[Math.abs((0 * 3 + 0) - (9 - 1)) = 8].value, last question with last answer
        -> config.quiz[Math.abs((0 * 3 + 1) - (9 - 1)) = 7].value, last question with second answer
        -> config.quiz[(0 * 3 + 2) - (9 - 1)) = 6].value, last question with first answer
     
        -> config.quiz[Math.abs((1 * 3 + 0) - (9 - 1)) = 5].value, second question with last answer
        -> config.quiz[(1 * 3 + 1) - (9 - 1)) = 4].value, second question with second answer
        -> config.quiz[(1 * 3 + 2) - (9 - 1)) = 3].value, second question with first answer
     
        -> config.quiz[Math.abs((2 * 3 + 0) - (9 - 1)) = 2].value, first question with last answer
        -> config.quiz[Math.abs((2 * 3 + 1) - (9 - 1)) = 1].value, first question with second answer
        -> config.quiz[Math.abs((2 * 3 + 2) - (9 - 1)) = 0].value, first question with first answer 
        */

        // Image snapshot
        //cy.dataCy('q-card').matchImageSnapshot(`q-card-back-btn-question-${questionIndex}`);

        // Back btn
        cy.dataCy('q-btn-back')
          .should('be.visible')
          .and('not.be.disabled')
          .and('have.text', i18n.global.t('backBtn'))
          .click();

        // Question
        let calcQuestionIdx =
          Math.abs(
            questionIndex * config.answersPerQuestion - config.quiz.length
          ) - config.answersPerQuestion;
        cy.dataCy('q-card-section-question')
          .should('be.visible')
          .get('div.text-h5')
          .should(
            'have.text',
            i18n.global.t(config.quiz[calcQuestionIdx].question)
          );

        // Question answers radio btn
        cy.get('[type="radio"]')
          .should('have.length', config.quiz.length / config.answersPerQuestion)
          .each(($el, answerIndex, $list) => {
            let calcQuestionIdx = Math.abs(
              questionIndex * config.answersPerQuestion +
                answerIndex -
                (config.quiz.length - 1)
            );
            let calcAnswerIdx =
              Math.abs(answerIndex - config.answersPerQuestion) - 1;
            if (config.quiz[calcQuestionIdx].value !== config.correctAnswer) {
              cy.wrap($list[calcAnswerIdx])
                .click({ force: true })
                .parent()
                .parent()
                .parent()
                .should('have.css', 'background-color', transparentColor)
                .contains(i18n.global.t(config.quiz[calcQuestionIdx].label));
            } else {
              // Correct answer
              cy.wrap($list[calcAnswerIdx])
                .click({ force: true })
                .parent()
                .parent()
                .parent()
                .should(
                  'have.css',
                  'background-color',
                  hexToRgb(config.secondaryColor)
                )
                .children()
                .contains(i18n.global.t(config.quiz[calcQuestionIdx].label));
            }
          });
        // Probressbar
        cy.dataCy('q-linear-progress')
          .should('be.visible')
          .get('div.q-badge')
          .should(
            'have.text',
            `${(calcQuestionIdx / config.quiz.length).toFixed(2) * 100}%`
          );
        // Next btn
        cy.dataCy('q-btn-next').should('be.visible').and('not.be.disabled');
        // Back btn
        if (calcQuestionIdx === 0) {
          cy.dataCy('q-btn-back')
            .should('be.visible')
            .and('be.disabled')
            .and('have.text', i18n.global.t('backBtn'));
        } else {
          cy.dataCy('q-btn-back')
            .should('be.visible')
            .and('not.be.disabled')
            .and('have.text', i18n.global.t('backBtn'));
        }
      });
    });
  });
});
