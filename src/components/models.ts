export interface Options {
  label: string;
  value: string;
  trans: string
}
