### Override global application config vars with SHELL/ENV vars

When you add some app new global var inside config file
`./quiz_config` you must add correspoding SHELL/ENV var here
`./src/utils/get_app_conf.js`.

Example global variable primaryColor (camelCase) inside config file
`./quiz_config` must have corresponding overriden SHEL/ENV var
`PRIMARY_COLOR` (upper case) inside `./src/utils/get_app_conf.js` file.
