const { parse } = require('toml');
const { readFileSync } = require('fs');

const getAppConfig = (process) => {
  const config = parse(readFileSync('./quiz_config.toml').toString());
  if (process.env.PRIMARY_COLOR) {
    config['primaryColor'] = process.env.PRIMARY_COLOR;
  } else if (process.env.SECONDARY_COLOR) {
    config['secondaryColor'] = process.env.SECONDARY_COLOR;
  } else if (process.env.TITLE_IMAGE) {
    config['titleImage'] = process.env.TITLE_IMAGE;
  } else if (process.env.TITLE_IMAGE_WIDTH) {
    config['titleImageWidth'] = process.env.TITLE_IMAGE_WIDTH;
  } else if (process.env.TITLE_IMAGE_HEIGTH) {
    config['titleImageHeigth'] = process.env.TITLE_IMAGE_HEIGTH;
  } else if (process.env.QUIZ_IMAGE) {
    config['quizImage'] = process.env.QUIZ_IMAGE;
  } else if (process.env.QUIZ_CARD_WIDTH) {
    config['quizCardWidth'] = process.env.QUIZ_CARD_WIDTH;
  } else if (process.env.QUIZ_TITLE) {
    config['quizTitle'] = process.env.QUIZ_TITLE;
  } else if (process.env.QUIZ_SUBTITLE) {
    config['quizSubtitle'] = process.env.QUIZ_SUBTITLE;
  } else if (process.env.ANSWER_PER_QUESTION) {
    config['answerPerQuestion'] = process.env.ANSWER_PER_QUESTION;
  } else if (process.env.CORRECT_ANSWER) {
    config['correctAnswer'] = process.env.CORRECT_ANSWER;
  } else if (process.env.SHOW_QUIT_APP_BTN) {
    config['showQuitAppBtn'] = process.env.SHOW_QUIT_APP_BTN;
  }
  return config;
};

module.exports.getAppConfig = getAppConfig;
